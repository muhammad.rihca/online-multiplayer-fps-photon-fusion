using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
using TMPro;

public class NetworkPlayer : NetworkBehaviour, IPlayerLeft
{
    public TextMeshProUGUI playerNickNameTM;

    public static NetworkPlayer Local { get; set; }

    public Transform playerModel;

    [Networked(OnChanged = nameof(OnNickNameChanged))]
    public NetworkString<_16> nickName { get; set; }

    // Remote client Token Hash
    [Networked] public int token { get; set; }

    bool isPublicJoinMessageSent = false;

    public LocalCameraHandler localCameraHandler;
    public GameObject localUI;

    // Other components
    NetworkInGameMessages networkInGameMessages;

    private void Awake()
    {
        networkInGameMessages = GetComponent<NetworkInGameMessages>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    public override void Spawned()
    {
        if (Object.HasInputAuthority)
        {
            Local = this;

            // Sets the layer of the local players model
            Utils.SetRenderLayerInChildren(playerModel, LayerMask.NameToLayer("LocalPlayerModel"));

            // Disable main camera
            if (Camera.main != null)
                Camera.main.gameObject.SetActive(false);

            // Enable 1 audio listener
            AudioListener audioListener = GetComponentInChildren<AudioListener>();
            audioListener.enabled = true;

            // Enable the local camera
            localCameraHandler.localCamera.enabled = true;

            // Detach camera if enabled
            localCameraHandler.transform.parent = null;

            // Enable UI for local camera
            localUI.SetActive(true);

            // RPC_SetNickName(PlayerPrefs.GetString("PlayerNickname"));
            RPC_SetNickName(GameManager.instance.playerNickName);

            Debug.Log("Spawned local Player");
        }
        else
        {
            // Disable the camera if we are not the local player
            // Camera localCamera = GetComponentInChildren<Camera>();
            // localCamera.enabled = false;

            // Disable the local camera for the remote players
            localCameraHandler.localCamera.enabled = false;

            // Disable UI for remote player
            localUI.SetActive(false);

            // Only 1 audio listener is allowed in the scene so disable remote player audio listener
            AudioListener audioListener = GetComponentInChildren<AudioListener>();
            audioListener.enabled = false;

            Debug.Log("Spawned remote Player");
        }

        // Set the player as a player object
        Runner.SetPlayerObject(Object.InputAuthority, Object);

        // Make it easier to tell which player in which
        transform.name = $"P_{Object.Id}";
    }

    public void PlayerLeft(PlayerRef player)
    {
        if (Object.HasStateAuthority)
        {
            // networkInGameMessages.SendInGameRPCMessage(nickName.ToString(), "left");

            if (Runner.TryGetPlayerObject(player, out NetworkObject playerLeftNetworkObject))
            {
                // networkInGameMessages.SendInGameRPCMessage(nickName.ToString(), "left");
                if (playerLeftNetworkObject == Object)
                {
                    Local.GetComponent<NetworkInGameMessages>().SendInGameRPCMessage(playerLeftNetworkObject.GetComponent<NetworkPlayer>().nickName.ToString(), "left");
                }
            }
        }

        if (player == Object.InputAuthority)
        {
            Runner.Despawn(Object);
        }
    }

    static void OnNickNameChanged(Changed<NetworkPlayer> changed)
    {
        Debug.Log($"{Time.time} OnNickNameChanged value {changed.Behaviour.nickName}");

        changed.Behaviour.OnNickNameChangedPrivate();
    }

    private void OnNickNameChangedPrivate()
    {
        Debug.Log($"Nickname changed for player to {nickName} for player {gameObject.name}");

        playerNickNameTM.text = nickName.ToString();
    }

    [Rpc(RpcSources.InputAuthority, RpcTargets.StateAuthority)]
    public void RPC_SetNickName(string nickName, RpcInfo Info = default)
    {
        Debug.Log($"[RPC] SetNickName {nickName}"); ;
        this.nickName = nickName;

        if (!isPublicJoinMessageSent)
        {
            networkInGameMessages.SendInGameRPCMessage(nickName, "joined");

            isPublicJoinMessageSent = true;
        }
    }

    private void OnDestroy()
    {
        // Gegt rid out the local camera if we get destroyed as a new one will be spawned with the new Network Player
        if (localCameraHandler != null)
        {
            Destroy(localCameraHandler.gameObject);
        }
    }
}
