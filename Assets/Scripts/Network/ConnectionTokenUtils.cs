using System;

public static class ConnectionTokenUtils
{
    public static byte[] NewToken() => Guid.NewGuid().ToByteArray();

    /// <param name="token"> token to be hashed </param>
    /// <returns>Token hash</returns>

    public static int HashToken(byte[] token) => new Guid(token).GetHashCode();

    /// <param name="token"> token to be parsed </param>
    /// <returns>Token as a string</returns>

    public static string TokenToString(byte[] token) => new Guid(token).ToString();
}
